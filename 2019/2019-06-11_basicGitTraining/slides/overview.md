# Overview

1. **PART I**: Quality of computer code.
2. **PART II**: Basic git course
    1. The terminal
    2. The editor
    3. What is `git`? What is the use of `git`? <!--(5 min)//-->
    4. Installation of `git`
    5. GitHub and GitLab <!--(5min)//-->
    6. How do I configure `git`? <!--(5min)//-->
    7. Where and how to start? <!--(5min)//-->
    8. What is a fork? <!--(5min)//-->
    9. What are branches?
    10. The 5 essential commands (`pull` / `status` / `add` / `commit` / `push`)
    11. What are merge/pull requests? <!--(10 min)//-->
    12. How do I synchronize my fork?
    13. Best practices