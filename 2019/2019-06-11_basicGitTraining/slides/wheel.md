# PART I

<br><br><h1>Quality of computer code.</h1><br>
<h1>Should you care? What if you didn't?</h1>



# A true story ...

<div align="center">
<img src="slides/img/wheel.png">
</div>

<table style="width:100%">
  <tr>
    <th width="33%" align="center">Researcher who cares about<br>quality of his code</th>
    <th width="33%" align="center">Fellow researcher</th>
    <th width="33%" align="center">Pi or Manager</th>
  </tr>
</table>