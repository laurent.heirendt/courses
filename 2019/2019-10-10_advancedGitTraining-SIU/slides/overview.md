# Overview

1. Installation and getting started

2. Ignoring files

3. Amend last commit

4. Resetting to a previous commit

5. Reverting commits

6. Rebasing in Git

7. Git cherry-picking

8. Merging branches

9. Conflict Resolution
