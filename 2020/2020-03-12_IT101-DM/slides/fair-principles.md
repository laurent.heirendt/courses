# FAIR (meta)data principles
 * dates back to 2014
 * well accepted by scientific community
 * necessity in data driven science
 * officially embraced by EU and G20
 * required by funding agencies and journal publishers
 
<center>
<img src="slides/img/fair-principles.png" height="400px">
</center>
<br>
<br>
