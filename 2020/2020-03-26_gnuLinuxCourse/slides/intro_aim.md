# Aims of the Tutorial

* Gain Familiarity With the Unix Shell
  - And hint at ways it can be harnessed to give great power to the
    user

<!-- .element: class="fragment" -->

* Connect to a GNU/Linux server
  + Using SSH, a de-facto standard for secure communication
	
<!-- .element: class="fragment" -->
<!-- <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/A-HIlNGczgU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
