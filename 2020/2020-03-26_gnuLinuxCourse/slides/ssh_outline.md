# SSH Exercise Outline

* Create an interactive SSH session

<!-- .element: class="fragment" -->
* Generate SSH key

<!-- .element: class="fragment" -->
* Use SSH key to log into the system

<!-- .element: class="fragment" -->
* Create a SSH tunnel

<!-- .element: class="fragment" -->
  
