# Problem solving
A guide for solving computing issues

1. Express the problem 
  * Write down what you want to achieve
2. Search for help 
  * Read **FAQs**, **help pages** and the **official documentation** well before turning to Google
  * Use stack exchange, forums and related resources (carefully)
3. Ask an expert 
    * You have to submit the problem in writing
    * Make the question interesting
