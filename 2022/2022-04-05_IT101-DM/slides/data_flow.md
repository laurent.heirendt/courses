# Typical flow of data

<div style="display:grid;grid-gap:10px;grid-template-columns: 30% 20% 30%;
  grid-auto-flow:column;grid-template-rows: repeat(4,auto);position:relative;left:8%">
  
<div class="content-box fragment" data-fragment-index="1">
  <div class="box-title red">Source data</div>
  <div class="content">

  * Experimental results
  * Large data sets  
  * Manually collected data  
  * External

  </div>
</div>

<div class="content-box fragment" data-fragment-index="2">
  <div class="box-title yellow">Intermediate</div>
  <div class="content">

  * Derived data 
  * Tidy data 
  * Curated sets

  </div>
</div>

<div class="content-box fragment" data-fragment-index="3">
  <div class="box-title blue">Analyses</div>
  <div class="content">

  * Exploratory
  * Model building
  * Hypothesis testing

  </div>
</div>

<div class="content-box fragment" data-fragment-index="4">
  <div class="box-title green">Dissemination</div>
  <div class="content">

  * Manuscript, report, presentation, ...

  </div>
</div>
<center>
<img src="slides/img/data-flow_sources.png" height=60%>
</center>
<center>
<img src="slides/img/data-flow_transformation.png" height=60%>
</center>
<center>
<img src="slides/img/data-flow_chart.png" height=60%>
</center>
<center>
<img src="slides/img/data-flow_paper.png" height=60%>
</center>

<div class="content-box fragment" data-fragment-index="5">
<div class="box-title red">Preserve</div>
  <div class="content">

  * Version data sets 
  * Backup
  * Protect
  
  </div>
</div>

<div class="content-box fragment" data-fragment-index="6">
  <div class="box-title yellow">Reproduce</div>
  <div class="content">

  * Automate your builds 
  * Use workflow tools (e.g. Snakemake)
  
  </div>
</div>


<div class="content-box fragment" data-fragment-index="7">
<div class="box-title blue">Trace</div>
  <div class="content">

 * Multiple iterations.
 * Code versioning (Git)

 </div>
</div>

<div class="content-box fragment" data-fragment-index="8">
<div class="box-title green">Track</div>
  <div class="content">

   * Through multiple versions
 
   </div>
</div>

</div>
<aside class="notes">
flow of the data is downstream (mostly), but you are going back and forth
applies to all data (financial report, lab safety assessment)
</aside>
